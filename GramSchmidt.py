# -*- coding: utf-8 -*-
"""
Created on Sat Dec 24 08:45:01 2016

@author: demerzel
"""
# python 2
#
# Final Project : Gram Schmidt Procedure, Python style
# Name: Karthik Mohan
#

# Program to work out the gram Schmidt procedure to obtain an orthonormal
# vector set from a random set of vectors
import math

# Real vectors


def dotpdtreal(k, l):
    """ This functions takes in two vectors, k and l, and returns their
        dot product
    """
    sum = 0
    for i in xrange(len(k)):
        sum += (k[i]*l[i])
    return sum


def normalizationreal(k):
    """ This function takes in an input List containing the coordinates of a
        vector and returns a list of coordinates of a normalized vector
    """
    norm = math.sqrt(dotpdtreal(k, k))
    return [x/norm for x in k]


def componentvectoreal(a, b):
    """ This function calculates the projection of b along a """
    firstep = dotpdtreal(a, b)
    secondstep = [x*firstep for x in a]
    return secondstep


def projectionlistgen(m, v):
    """ Inputs : m - List of lists, v - list (vector coord.)
        This function generates the projection components of v among the
        list of vectors in m
        Output : List of lists containing projection components """
    projeclis = []
    for i in xrange(len(m)):
        projeclis += [componentvectoreal(m[i], v)]
    return projeclis


def projectionrem(m, v):
    """ Input m - list of projections of various vectors on vector, v
        Output : Unit vector """
    if len(m) != 1:
        netdiff = [sum(i) for i in zip(*m)]
    else:
        netdiff = m[0]
    overall = []
    for i in xrange(len(v)):
        overall += [v[i] - netdiff[0][i]]
    return normalizationreal(overall)


def stringtofloat(m):
    """ This function converts the list of lists, m, that has the vectors
        in string format to float format """
    a = []
    b = []
    for i in m:
        for j in i:
            a += [j.split(",")]
    for i in a:
        b += [[float(j) for j in i]]
    return b

# Vectors with complex components


def dotpdtim(x, y):
    """ This function defines the dot product of vectors represented by
        complex numbers """
    sum = 0
    for i in xrange(len(y)):
        sum += ((x[i].conjugate())*y[i])
    return sum


def normalizationim(k):
    """ This function takes in an input List containing the coordinates of a
        vector and returns a list of coordinates of a normalized vector
    """
    norm = math.sqrt(dotpdtim(k, k).real)
    return [x/norm for x in k]


def componentvectorim(a, b):
    """ This function calculates the projection of b along a """
    firstep = dotpdtim(a, b)
    secondstep = [x*firstep for x in a]
    return secondstep


def projectionlistgenim(m, v):
    """ Inputs : m - List of lists, v - list (vector coord.)
        This function generates the projection components of v among the
        list of vectors in m
        Output : List of lists containing projection components """
    projeclis = []
    for i in xrange(len(m)):
        projeclis += [componentvectorim(m[i], v)]
    return projeclis


def projectionremim(m, v):
    """ Input m - list of projections of various vectors on vector, v
        Output : Unit vector """
    if len(m) != 1:
        netdiff = [sum(i) for i in zip(*m)]
    else:
        netdiff = m[0]
    overall = []
    for i in xrange(len(v)):
        overall += [v[i] - netdiff[0][i]]
    return normalizationim(overall)


def stringtocomplex(x, y):
    """ This function takes in the split coordinates and rejoins them
        to form complex numbers """
    m = []
    n = []
    final = []
    for i in x:
        for j in i:
            m += [j.split(",")]
    for i in y:
        for j in i:
            n += [j.split(",")]
    for i in xrange(len(m)):
        final += [map(lambda a, b: complex(int(a), int(b)), m[i], n[i])]
    return final


def start():
    """ This function helps with human interaction """
    print " Gram-Schmidt Procedure "
    print " Choose - Real component Vectors or Complex component vectors "
    print " Enter R for real and I for imaginary (complex) "
    choie = raw_input(" Enter your choice: ")
    if choie == 'R':
        nos = raw_input(" Please enter the number of dimensions: ")
        coords = []
        print " Enter the coordinates of the vectors separated by a ','\n"
        print " For ex: In 3 dimensions - 5,2,1 \n"
        print " Pay attention to the example and follow format(no spaces)"
        print " The number of coordinates should match the dimension"
        for i in xrange(1, (int(nos)+1)):
            coords += [[raw_input(" Enter coordinates of vector %d : " % i)]]
        propercoords = stringtofloat(coords)
        orthoset = []
        orthoset += [normalizationreal(propercoords[0])]
        for i in propercoords[1:]:
            projections = []
            projections += [projectionlistgen(orthoset, i)]
            orthoset += [projectionrem(projections, i)]
        for i in orthoset:
            print "\n"
            print i
        return "These are the required coordinates"
    else:
        nos = raw_input(" Please enter the number of dimensions: ")
        realpart = []
        imaginarypart = []
        print " Enter the real part of the vectors separated by a ','\n"
        print " Followed by the complex part "
        print " For ex: In 2 dimensions if the vectors are \n"
        print " Vector 1 : (6+5i),(1+2i) And Vector 2 : (4+2i),(1+3i)"
        print " Then enter the real parts of the first vector, i.e. 6,1"
        print " Followed by the imaginary parts, i.e. 5,2"
        print " Pay attention to the example and follow format(no spaces)"
        print " The number of coordinates should match the dimension"
        print " If there are no imaginary parts, enter 0 in that case"
        for i in xrange(1, (int(nos)+1)):
            realpart += [[raw_input(" Enter real parts of vector %d : " % i)]]
            imaginarypart += [[raw_input(" Enter imaginary parts of vector %d : " % i)]]
        propercoords = stringtocomplex(realpart, imaginarypart)
        orthoset = []
        orthoset += [normalizationim(propercoords[0])]
        for i in propercoords[1:]:
            projections = []
            projections += [projectionlistgenim(orthoset, i)]
            orthoset += [projectionremim(projections, i)]
        for i in orthoset:
            print "\n"
            print i
        return "These are the required coordinates"