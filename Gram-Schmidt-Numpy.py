# -*- coding: utf-8 -*-
"""
Created on Sat Feb  4 07:35:27 2017

@author: demerzel
"""

import numpy
import math
import warnings
warnings.filterwarnings('ignore')


def normalization(k):
    """ This function normalizes the vector k """
    return k/math.sqrt((numpy.vdot(k, k)))


def componentvectors(a, b):
    """ This function calculates the projection of b along a """
    firstep = numpy.vdot(a, b)
    return firstep*a


def componentlist(k, v):
    """ Input, k : An array containing the vectors in question
        v : the vector whose component w.r.t other vectors needs to be measured
        Output : array with components which can be subtracted out of v """
    component = numpy.zeros((len(k), len(k)))
    for i in xrange(len(k)):
        component[i] = componentvectors(k[i], v)
    return component


def componentrem(k, v):
    """ This function removes the previously determined components of v in other
        vectors, from v and then normalizes v """
    l = numpy.sum(k, axis=0)
    m = (v - l)
    return normalization(m)


def start():
    """ This function starts the procedure """
    print "Gram Schmidt Procedure \n"
    n = input("Enter the number of dimensions as an integer : ")
    assert type(n) is int
    incords = numpy.zeros((n, n), dtype=complex)
    orthoset = numpy.zeros((n, n), dtype=complex)
    for i in xrange(n):
        print "Enter the coordinates of vector no. ", (i+1)
        for j in xrange(n):
            incords[i][j] = input("Enter component no. %d: " % (j+1))
    orthoset[0] = normalization(incords[0])
    for i in xrange(1, n):
        projections = componentlist(orthoset, incords[i])
        orthoset[i] = componentrem(projections, incords[i])
    for i in orthoset:
        print "\n"
        print i
    return "These are the coordinates"
