# README #

Gram Schmidt Procedure
* Gram Schmidt Procedure is used to generate orthonormal basis vectors from randomly oriented vectors. This has ever present uses, ranging from Quantum Mechanics to General Relativity. 
* These codes exploit lists of lists as one method and numpy as another to calculate the orthonormal basis. Numpy version is faster while the list of lists version is slow but informative.

Set-up

* The program can be run by typing "start()" (without quotes) in the console window. 
* Requires numpy and math modules
* Numpy version is the go to code for quick calculations.

**Owner : Karthik Mohan**
***email: karthik.mohan390@gmail.com***